<h2>Hi guys...</h2>

<h5>
It is an online chat project written with HTML and you can easily add it to your site.

</h5>
<p align="center"><img src="img/chat-online.png" /></p>

```

onlineChat({
    title: 'راه های ارتباطی ما',
    links: {
        whatsapp: {
            title: 'واتساپ',
            subTitle: 'بخش فروش واتساپ',
            link: 'https://wa.me/Number',

        },
        telegram: {
            title: 'تلگرام',
            subTitle: 'بخش توسعه دهندگان',
            link: 'https://t.me/id',
        },
        Email: {
            title: 'ایمیل',
            subTitle: 'ارتباط با ما',
            link: 'email@email.com',
        },
        Call: {
            title: 'تماس',
            subTitle: 'تماس تلفنی در ساعات اداری',
            link: 'tel: creates the call link',
        },
        instagram: {
            title: 'اینستاگرام',
            subTitle: 'ما را دنبال کنید',
            link: 'http://www.instagram.com/yourusername',
        },
    }
});

```
